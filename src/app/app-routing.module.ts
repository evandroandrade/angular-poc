import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HoraAtualComponent } from './hora-atual/hora-atual.component';

const routes: Routes = [{ path: 'hora-atual', component: HoraAtualComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
