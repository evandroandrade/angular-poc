import { getLocaleDateTimeFormat } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hora-atual',
  templateUrl: './hora-atual.component.html',
  styleUrls: ['./hora-atual.component.scss'],
})
export class HoraAtualComponent implements OnInit {
  public now: Date = new Date();
  @Input() name: string | undefined;

  constructor() {}

  ngOnInit(): void {
    setInterval(() => {
      this.now = new Date();
    }, 1);
  }
}
